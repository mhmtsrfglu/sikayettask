-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Anamakine: localhost:8889
-- Üretim Zamanı: 14 May 2019, 17:01:43
-- Sunucu sürümü: 5.6.38
-- PHP Sürümü: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `sikayetvar`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `results`
--

CREATE TABLE `results` (
  `id` int(11) NOT NULL,
  `visitdate` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `deviceCategory` varchar(50) NOT NULL,
  `sessions` varchar(50) NOT NULL,
  `hits` varchar(50) NOT NULL,
  `pageview` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `results`
--

INSERT INTO `results` (`id`, `visitdate`, `city`, `deviceCategory`, `sessions`, `hits`, `pageview`) VALUES
(1, '20190507', '(not set)', 'mobile', '2', '7', '7'),
(2, '20190507', 'Ankara', 'desktop', '2', '5', '5'),
(3, '20190507', 'Ankara', 'mobile', '2', '7', '7'),
(4, '20190507', 'Arnhem', 'desktop', '1', '5', '5'),
(5, '20190507', 'Istanbul', 'desktop', '4', '11', '11'),
(6, '20190507', 'Istanbul', 'mobile', '2', '3', '3'),
(7, '20190507', 'Izmir', 'mobile', '1', '2', '2'),
(8, '20190507', 'Karabuk', 'mobile', '2', '2', '2'),
(9, '20190507', 'Yalova', 'desktop', '2', '3', '3'),
(10, '20190508', 'Adana', 'mobile', '2', '5', '5'),
(11, '20190508', 'Ankara', 'desktop', '2', '3', '3'),
(12, '20190508', 'Antalya', 'desktop', '2', '7', '7'),
(13, '20190508', 'Edirne', 'desktop', '1', '3', '3'),
(14, '20190508', 'Gaziantep', 'mobile', '1', '1', '1'),
(15, '20190508', 'Istanbul', 'desktop', '5', '18', '18'),
(16, '20190508', 'Istanbul', 'mobile', '2', '4', '4'),
(17, '20190508', 'Izmir', 'desktop', '1', '1', '1'),
(18, '20190508', 'Sakarya', 'desktop', '1', '1', '1'),
(19, '20190509', 'Ankara', 'desktop', '1', '1', '1'),
(20, '20190509', 'Bursa', 'mobile', '1', '3', '3'),
(21, '20190509', 'Istanbul', 'desktop', '6', '16', '16'),
(22, '20190509', 'Izmir', 'desktop', '1', '2', '2'),
(23, '20190509', 'Izmir', 'mobile', '3', '7', '7'),
(24, '20190509', 'Kirsehir', 'desktop', '2', '2', '2'),
(25, '20190509', 'Malatya', 'desktop', '1', '1', '1'),
(26, '20190509', 'Moscow', 'mobile', '1', '4', '4'),
(27, '20190509', 'Sakarya', 'desktop', '1', '5', '3'),
(28, '20190509', 'Seoul', 'desktop', '1', '3', '3'),
(29, '20190510', 'Ankara', 'desktop', '1', '1', '1'),
(30, '20190510', 'Ankara', 'mobile', '1', '19', '10'),
(31, '20190510', 'Antalya', 'mobile', '1', '2', '2'),
(32, '20190510', 'Bursa', 'mobile', '1', '5', '5'),
(33, '20190510', 'Erlangen', 'desktop', '1', '1', '1'),
(34, '20190510', 'Istanbul', 'desktop', '3', '8', '8'),
(35, '20190510', 'Konya', 'desktop', '1', '1', '1'),
(36, '20190511', 'Ankara', 'desktop', '1', '7', '7'),
(37, '20190511', 'Ankara', 'mobile', '1', '2', '2'),
(38, '20190511', 'Istanbul', 'desktop', '1', '1', '1'),
(39, '20190511', 'Istanbul', 'mobile', '5', '11', '11'),
(40, '20190511', 'Konya', 'desktop', '1', '5', '5'),
(41, '20190511', 'Munich', 'mobile', '1', '1', '1'),
(42, '20190512', 'Istanbul', 'desktop', '1', '2', '2'),
(43, '20190512', 'Istanbul', 'mobile', '1', '1', '1'),
(44, '20190513', 'Ankara', 'desktop', '1', '2', '2'),
(45, '20190513', 'Bodrum', 'desktop', '1', '3', '3'),
(46, '20190513', 'Edirne', 'desktop', '2', '4', '4'),
(47, '20190513', 'Istanbul', 'desktop', '2', '4', '4'),
(48, '20190513', 'Istanbul', 'mobile', '2', '5', '5'),
(49, '20190513', 'Izmir', 'desktop', '3', '6', '6'),
(50, '20190513', 'Izmir', 'mobile', '3', '4', '4'),
(51, '20190513', 'Osaka', 'mobile', '1', '1', '1'),
(52, '20190514', 'Agri', 'desktop', '1', '2', '2'),
(53, '20190514', 'Ankara', 'desktop', '1', '1', '1'),
(54, '20190514', 'Colombo', 'desktop', '1', '1', '1'),
(55, '20190514', 'Istanbul', 'desktop', '4', '19', '19'),
(56, '20190514', 'Samsun', 'desktop', '1', '1', '1'),
(57, '20190514', 'Sao Paulo', 'desktop', '1', '1', '1');

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `results`
--
ALTER TABLE `results`
  ADD PRIMARY KEY (`id`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `results`
--
ALTER TABLE `results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
