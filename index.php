<?php

require __DIR__."/db.php";
require_once __DIR__ . '/vendor/autoload.php';

$analytics = initializeAnalytics();

$response = getReport($analytics);

printResults($response);



function initializeAnalytics()
{
    // Create and configure a new client object.
    $client = new Google_Client();
    $client->setApplicationName("Hello SikayetVar");
    $client->setAuthConfig(__DIR__."/sikayetvar-ga-task-7682a0220b9f.json");
    $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
    $analytics = new Google_Service_AnalyticsReporting($client);

    return $analytics;
}


function getReport($analytics) {
    // Replace with your view ID, for example XXXX.
    $VIEW_ID = "176638876";

    // Create the DateRange object.
    $dateRange = new Google_Service_AnalyticsReporting_DateRange();
    $dateRange->setStartDate("7daysAgo");
    $dateRange->setEndDate("today");

    // Create the Metrics object.
    $sessions = new Google_Service_AnalyticsReporting_Metric();
    $sessions->setExpression("ga:sessions");
    $sessions->setAlias("sessions");

    // Create the Metrics object.
    $hits = new Google_Service_AnalyticsReporting_Metric();
    $hits->setExpression("ga:hits");
    $hits->setAlias("hits");
    // Create the Metrics object.
    $pageviews = new Google_Service_AnalyticsReporting_Metric();
    $pageviews->setExpression("ga:pageviews");
    $pageviews->setAlias("pageview");

    //Create the Dimensions object.
    $date = new Google_Service_AnalyticsReporting_Dimension();
    $date->setName("ga:date");

    //Create the Dimensions object.
    $city = new Google_Service_AnalyticsReporting_Dimension();
    $city->setName("ga:city");

    //Create the Dimensions object.
    $deviceCategory = new Google_Service_AnalyticsReporting_Dimension();
    $deviceCategory->setName("ga:deviceCategory");

    // Create the ReportRequest object.
    $request = new Google_Service_AnalyticsReporting_ReportRequest();
    $request->setViewId($VIEW_ID);
    $request->setDateRanges($dateRange);
    $request->setMetrics(array($sessions,$hits,$pageviews));
    $request->setDimensions(array($date,$city,$deviceCategory));




    $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
    $body->setReportRequests( array( $request) );
    return $analytics->reports->batchGet( $body );
}


/**
 * Parses and prints the Analytics Reporting API V4 response.
 *
 * @param An Analytics Reporting API V4 response.
 */
function printResults($reports) {
    for ( $reportIndex = 0; $reportIndex < count( $reports ); $reportIndex++ ) {
        $report = $reports[ $reportIndex ];
        $header = $report->getColumnHeader();
        $dimensionHeaders = $header->getDimensions();
        $metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
        $rows = $report->getData()->getRows();

        for ( $rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
            $row = $rows[ $rowIndex ];
            $dimensions = $row->getDimensions();
            $metrics = $row->getMetrics();
            $arr = array();
            for ($i = 0; $i < count($dimensionHeaders) && $i < count($dimensions); $i++) {
                $arr[$dimensionHeaders[$i]] = $dimensions[$i];
            }

            for ($j = 0; $j < count($metrics); $j++) {
                $values = $metrics[$j]->getValues();
                for ($k = 0; $k < count($values); $k++) {
                    $entry = $metricHeaders[$k];
                    $arr[$entry->getName()] = $values[$k];
                }
            }

            store($arr);
        }
    }
}

function store($arr){
    global $dbh;

    $insert = $dbh->prepare("insert into results(visitdate,city,deviceCategory,sessions,hits,pageview) values
                                                              (:dt,:city,:dcategory,:ses,:hits,:pageview)");
    $insert->bindParam(":dt",$arr["ga:date"],PDO::PARAM_STR);
    $insert->bindParam(":city",$arr["ga:city"],PDO::PARAM_STR);
    $insert->bindParam(":dcategory",$arr["ga:deviceCategory"],PDO::PARAM_STR);
    $insert->bindParam(":ses",$arr["sessions"],PDO::PARAM_STR);
    $insert->bindParam(":hits",$arr["hits"],PDO::PARAM_STR);
    $insert->bindParam(":pageview",$arr["pageview"],PDO::PARAM_STR);

    if($insert->execute()){
        echo "Date : ".$arr["ga:date"]."</br>";
        echo "City : ".$arr["ga:city"]."</br>";
        echo "Device Category : ".$arr["ga:deviceCategory"]."</br>";
        echo "Session : ".$arr["sessions"]."</br>";
        echo "Hit : ".$arr["hits"]."</br>";
        echo "Page View : ".$arr["pageview"]."</br>"."---------------------------"."</br>";
    }else{
        print_r($insert->errorInfo());
        echo "<br>";
    }


}